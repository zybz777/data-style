# 数据集格式

    此仓库用来生成 yolo 所使用的自定数据集。

## 1. 文件树

*parent*

*├── Annotations* :数据集的标签

*└── images*：数据集

*└── ImageSets*：训练集、验证集、测试集

*└── labels*：训练集、验证集、测试集的标签集合

*└── userfunc*：处理数据集的 python 文件

*└──parent.yaml* ：数据集调用文件

## 2. 制作数据集

- 将标签集合放入 **Annotations**中
- 将图片集合放入 **images** 中

## 3. 使用说明

```shell
cd userfunc
python run.py # 指定 run.py 中 classes 的类别
```

- 将 XXX.yaml 文件复制到 yolov5/data/ 路径里
- 将本数据集放入与 yolo 文件夹同级的 datasets 文件夹下

  *parent1*

  *├── yolov5* :代码文件

  *└── datasets*：数据集

## 4. 额外功能

DLC_XXX.py 提供了一些辅助功能

- DLC_rename.py：按提供的后缀名批量修改文件名
- DLC_video_and_picture.py: 从摄像头拍摄视频和将视频拆图
- DLC_xml_change.py: 修改xml文件内指定内容 (未完善)
