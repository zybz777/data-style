# author zybz 2022.3.16
# DLC 重命名数据集
import os


class BatchRename:
    def rename_file_by_suffix(self, file_suffix, folder_path, jpg_name, id_begin=1):
        """Batch rename files

        Args:
            file_suffix (string): the file suffix such as 'jpg', 'png', 'xml'
            folder_path (string): The folder address
            jpg_name (string): the file name you wish to rename such as 'block', you will get 'block_0001.jpg'
            id_begin (int): The beginning of the file name. Defaults to 1.
        """
        path = folder_path
        filelist = os.listdir(path)
        total_num = len(filelist)
        i = int(id_begin)
        file_suffix = "." + file_suffix
        for item in filelist:
            if item.endswith(file_suffix):
                src = os.path.join(os.path.abspath(path), item)
                dst = os.path.join(os.path.abspath(path), jpg_name + "_" + str(i).zfill(4) + file_suffix)  # 可根据自己需求选择格式
                try:
                    os.rename(src, dst)  # src:原名称  dst新名称d
                    i += 1
                except:
                    continue
        print("total %d to rename & converted %d " % (total_num, i - 1) + file_suffix)


if __name__ == "__main__":
    demo = BatchRename()
    demo.rename_file_by_suffix('jpg', folder_path='images/block1_down', jpg_name='block', id_begin=1)
