# author zybz 2022.3.2
""" Usage:
        shell: cd userfunc
        please input classes
        shell: python run.py
"""
from make_imageSets import get_imggeSets
from make_labels import get_labels
from make_yaml import get_yaml

if __name__ == "__main__":
    get_imggeSet = get_imggeSets()  # step 1
    classes = ['fire', 'fall']
    get_label = get_labels(classes)  # step 2
    get_yaml = get_yaml(classes, copy=True, copy_dst_path='robocon-vision/data')  # step 3: generate yaml file
