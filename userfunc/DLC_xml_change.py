# author zybz 2022.3.16
# DLC 修改错误标签
import xml.etree.ElementTree as ET
import os


# 批量修改整个文件夹所有的xml文件
def change_all_xml(xml_path):
    filelist = os.listdir(xml_path)
    print(filelist)
    # 打开xml文档
    for xmlfile in filelist:
        doc = ET.parse(xml_path + xmlfile)
        root = doc.getroot()
        objs = root.findall("object")  # 找到 obj 标签
        for obj in objs:
            name = obj.find("name")
            if "bule" in name.text:
                name.text = name.text.replace("bule", "blue")
                print(name.text)

        doc.write(xml_path + xmlfile)  # 保存修改


# 修改某个特定的xml文件
def change_one_xml(xml_path):  # 输入的是这个xml文件的全路径
    # 打开xml文档
    doc = ET.parse(xml_path)
    root = doc.getroot()
    objs = root.findall("object")  # 找到 obj 标签
    for obj in objs:
        name = obj.find("name")
        if "bule" in name.text:
            name.text = name.text.replace("bule", "blue")
            print(name.text)
    doc.write(xml_path)  # 保存修改
    print("----------done--------")


if __name__ == "__main__":
    xml_path = "../Annotations/"
    change_all_xml(xml_path)
